import express from "express";
import { IPosition } from "../contracts/Itable";
import { GameLoop } from "./GameLoop";

const app = express();

app.use(express.json());

const colors: string[] = ["blue", "red", "yellow"];
const initialValue: IPosition = { x: 0, y: 0 };
const dimensions: number = 4;

const game = new GameLoop(dimensions, colors, initialValue);

game.runGame();

export { app };
