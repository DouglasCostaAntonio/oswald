import { IPosition } from "../contracts/Itable";
import { Table } from "./Table";

export class GameLoop {
  public table: any;
  public sequence: string[];

  constructor(dimensions: number, colors: string[], initialValue: IPosition) {
    this.table = new Table(dimensions, colors, initialValue);
    this.table.makeTable();
    this.sequence = [];
  }

  setGameStatistics(): void {
    this.sequence.push(this.table.getNextColor());
    console.info("color", this.table.getNextColor());
    console.info("STEP", this.sequence.length);
  }

  showStatistics(): void {
    console.info(this.sequence.join(" > "));
  }

  runGame(): void {
    console.table(this.table.table);

    do {
      this.table.fetchSteps();
      this.setGameStatistics();

      console.table(this.table.table);

    } while (this.endGame());

    this.showStatistics();
  }

  endGame(): boolean {
    return (
      this.table.positions.length <
      this.table.dimensions * this.table.dimensions
    );
  }
}
