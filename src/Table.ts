import { IPosition } from "../contracts/Itable";

export class Table {
  public dimensions: number;
  public colors: string[];
  public table: any;
  public positions: IPosition[];
  public colorPositions: any;
  public neighborFound = false;

  constructor(dimensions: number, colors: string[], initialValue: IPosition) {
    this.dimensions = dimensions;
    this.colors = colors;
    this.positions = [initialValue];
  }

  getColor(): string {
    return this.colors[Math.floor(Math.random() * this.colors.length)];
  }

  makeTable(): void {
    const table: any = [];

    for (let count = 0; count < this.dimensions; count++) {
      table.push(
        Array.from({ length: this.dimensions }, (v, k) => this.getColor())
      );
    }

    this.table = table;
  }

  getNextColor(): string {
    let nextPath: any = [];
    let nextColor: string = "";

    this.colors.forEach((key: string) => {
      if (this.colorPositions[key] > nextPath) {
        nextPath = this.colorPositions[key];
        nextColor = key;
      }
    });

    return nextColor;
  }

  buildTuple(position: IPosition, nextColor: string | null): void {
    const color = this.table[position.y][position.x];

    if (nextColor !== null && nextColor !== color) {
      return;
    }

    if (!this.positions.some((e) => e.x === position.x && e.y === position.y)) {
      this.colorPositions[color].push(position);
      this.positions.push(position);
      this.neighborFound = true;
    }
  }

  fetchStepX(position: IPosition, nextColor: string | null): void {
    if (position.x + 1 >= this.dimensions) {
      return;
    }

    this.buildTuple({ ...position, x: position.x + 1 }, nextColor);
  }

  fetchStepY(position: IPosition, nextColor: string | null): void {
    if (position.y + 1 >= this.dimensions) {
      return;
    }

    this.buildTuple({ ...position, y: position.y + 1 }, nextColor);
  }

  changeColors(nextColor: string): void {
    this.positions.map((position: IPosition) => {
      this.table[position.y][position.x] = nextColor;
    });
  }

  fetchNeighbor(nextColor: string) {
    this.neighborFound = false;

    this.positions.map((position: IPosition) => {
      this.fetchStepX(position, nextColor);
      this.fetchStepY(position, nextColor);
    });

    if (this.neighborFound) {
      this.fetchNeighbor(nextColor);
    }
  }

  fetchSteps(): void {
    this.colorPositions = Object.assign(
      {},
      ...Object.entries(this.colors).map(([a, b]) => ({ [b]: [] }))
    );

    this.positions.map((position: IPosition) => {
      this.fetchStepX(position, null);
      this.fetchStepY(position, null);
    });

    this.fetchNeighbor(this.getNextColor());

    this.changeColors(this.getNextColor());
  }
}
