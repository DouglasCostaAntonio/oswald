import { Table } from "../src/Table";
import { expect } from "chai";
import { IPosition } from "../contracts/Itable";

describe("table with 3 rows", () => {
  const colors: string[] = ["blue", "red", "yellow"];
  const dimensions: number = 3;
  const initialValue: IPosition = { x: 0, y: 0 };

  const table = new Table(dimensions, colors, initialValue);

  table.makeTable();

  it(`needs to contains one of that [${colors.join(", ")}]`, () => {
    expect(colors).to.include(table.getColor());
  });

  it(`needs to have ${dimensions * dimensions} elements`, () => {
    const flatTable: string[] = [].concat(...table.table);
    expect(flatTable).to.length(dimensions * dimensions);
  });
});

describe("table with 4 rows", () => {
  const colors: string[] = ["black", "white", "gold"];
  const dimensions: number = 4;
  const initialValue: IPosition = { x: 0, y: 0 };

  const table = new Table(dimensions, colors, initialValue);

  table.makeTable();

  it(`needs to contains one of that [${colors.join(", ")}]`, () => {
    expect(colors).to.include(table.getColor());
  });

  it(`needs to have ${dimensions * dimensions} elements`, () => {
    const flatTable: string[] = [].concat(...table.table);
    expect(flatTable).to.length(dimensions * dimensions);
  });
});

describe("when we fetch a path", () => {
  const colors: string[] = ["black", "white", "gold"];
  const dimensions: number = 4;
  const initialValue: IPosition = { x: 0, y: 0 };

  const table = new Table(dimensions, colors, initialValue);

  table.makeTable();

  table.fetchSteps();

  it(`after fetch, we must have more than 1 position`, () => {
    expect(table.positions.length).to.greaterThan(1);
  });

  it(`and the color should be equal`, () => {
    expect(table.getNextColor() === table.table[0][0]).to.be.true;
  });
});
