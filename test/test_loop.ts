import { GameLoop } from "../src/GameLoop";
import { expect } from "chai";
import { IPosition } from "../contracts/Itable";

describe("running a controlled example", () => {
  const colors: string[] = ["blue", "red", "yellow"];
  const initialValue: IPosition = { x: 0, y: 0 };
  const dimensions: number = 4;
  const sequence: string[] = ["blue", "yellow", "blue", "red"];

  const game = new GameLoop(dimensions, colors, initialValue);

  // controlled value for test
  game.table.table = [
    ["blue", "red", "yellow", "yellow"],
    ["blue", "red", "yellow", "blue"],
    ["yellow", "yellow", "red", "yellow"],
    ["blue", "blue", "red", "red"],
  ];

  game.runGame();

  it(`the qty of steps should be 4`, () => {
    expect(game.sequence.length === 4).to.be.true;
  });

  it(`the sequence of steps should be like that (${sequence.join(
    " > "
  )})`, () => {
    expect(game.sequence.join(" > ") === sequence.join(" > ")).to.be.true;
  });
});
